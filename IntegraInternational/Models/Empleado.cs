﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace IntegraInternational.Models
{
    public class Empleado
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "Nombre requerido")]
        [StringLength(50)]
        [Display(Name = "Nombre", Order = -9,
        Prompt = "Ingrese el nombre", Description = "Emp Name")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Apellido Apellido")]
        [StringLength(50)]
        [Display(Name = "Apellido", Order = -9,
        Prompt = "Ingrese el Apellido", Description = "Emp Name")]
        public string Apellido { get; set; }

        [Required(ErrorMessage = "Telefono requerido")]
        [StringLength(15)]
        [Display(Name = "Telefono", Order = -9,
        Prompt = "Ingrese el Telefono", Description = "Emp Name")]
        [Range(0, double.MaxValue, ErrorMessage = "Ingrese un numero telefonico valido")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "Correo requerido")]
        [StringLength(100)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "Correo", Order = -9,
        Prompt = "Ingrese el Correo", Description = "Emp Name")]
        public string Correo { get; set; }

        public string? Foto { get; set; }

        [Required(ErrorMessage = "Foto requerida")]
        [NotMapped]
        public IFormFile ArchivoImagen { get; set; }


        [Required(ErrorMessage = "Fecha de Ingreso requerida")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha de Ingreso")]
        public DateTime FechaIngreso { get; set; }
    }
}
