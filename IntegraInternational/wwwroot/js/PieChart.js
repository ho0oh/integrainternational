﻿$(document).ready(function () {
    $.ajax({
        type: "GET",
        contentType: "application/jason; charset=utf-8",
        dataType: "json",
        url: urlbase+"/DataPieChar",
        error: function () {
            alert("Ocurrio un error al consultar los datos");
        },
        success: function (data) {
            PieChart(data);
        }
    })
});

function PieChart(data) {
    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Grafica con Json de Api consumida'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Valores',
            colorByPoint: true,
            data: data
        }]
    });
}
