﻿using IntegraInternational.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace IntegraInternational.Controllers
{
    public class DashBoardController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        //retorna lista para dahsboard con formato json
        public JsonResult DataPieChar()
        {
            PieChar pieChar = new PieChar();
            return Json(GetDataDummy());
        }

        //Consumo de api
        public List<PieChar> GetDataDummy()
        {
            string url = "https://api.coindesk.com/v1/bpi/currentprice.json";
            WebClient wc = new WebClient();
            var json = wc.DownloadString(url);
            var rs = JsonConvert.DeserializeObject<Root>(json);
            List<PieChar> pieChars = new List<PieChar>();
            pieChars.Add(new PieChar(rs.bpi.USD.description, rs.bpi.USD.rate_float));
            pieChars.Add(new PieChar(rs.bpi.EUR.description, rs.bpi.EUR.rate_float));
            pieChars.Add(new PieChar(rs.bpi.GBP.description, rs.bpi.GBP.rate_float));

            return pieChars;
        }
    }
}
