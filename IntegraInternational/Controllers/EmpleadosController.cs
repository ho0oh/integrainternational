﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IntegraInternational;
using IntegraInternational.Models;
using System.Web;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Rotativa.AspNetCore;

namespace IntegraInternational.Controllers
{
    public class EmpleadosController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;

        public EmpleadosController(ApplicationDbContext context, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            this._hostEnvironment = hostEnvironment;
        }

        // GET: Empleados
        public async Task<IActionResult> Index()
        {
            return View(await _context.empleados.ToListAsync());
        }

        [HttpGet]
        public async Task<IActionResult> Index(string valBusquedaEmpleado)
        {
            ViewData["GetBuscaEmpleado"] = valBusquedaEmpleado;
            var varBusquedaEmpleado = from x in _context.empleados select x;
            if (!String.IsNullOrEmpty(valBusquedaEmpleado))
            {
                varBusquedaEmpleado = varBusquedaEmpleado.Where(x => x.Apellido.Contains(valBusquedaEmpleado) || x.Correo.Contains(valBusquedaEmpleado));
            }
            return View(await varBusquedaEmpleado.AsNoTracking().ToListAsync());
        }

        // GET: Empleados/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var empleado = await _context.empleados
                .FirstOrDefaultAsync(m => m.id == id);
            if (empleado == null)
            {
                return NotFound();
            }

            return View(empleado);
        }

        // GET: Empleados/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Empleados/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,Nombre,Apellido,Telefono,Correo,ArchivoImagen,FechaIngreso")] Empleado empleado)
        {
            #region se recibe archivo de imagen
            string wwwRootPath = _hostEnvironment.WebRootPath;
            string fileName = Path.GetFileNameWithoutExtension(empleado.ArchivoImagen.FileName);
            string extension = Path.GetExtension(empleado.ArchivoImagen.FileName);
            empleado.Foto = fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            string path = Path.Combine(wwwRootPath + "/image", fileName);
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await empleado.ArchivoImagen.CopyToAsync(fileStream);
            }
            #endregion

            if (ModelState.IsValid)
            {
                _context.Add(empleado);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(empleado);
        }

        // GET: Empleados/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var empleado = await _context.empleados.FindAsync(id);
            if (empleado == null)
            {
                return NotFound();
            }
            return View(empleado);
        }

        // POST: Empleados/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,Nombre,Apellido,Telefono,Correo,ArchivoImagen,FechaIngreso")] Empleado empleado)
        {
            if (empleado.ArchivoImagen != null)
            {
                #region se recibe archivo de imagen
                string wwwRootPath = _hostEnvironment.WebRootPath;
                string fileName = Path.GetFileNameWithoutExtension(empleado.ArchivoImagen.FileName);
                string extension = Path.GetExtension(empleado.ArchivoImagen.FileName);
                empleado.Foto = fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                string path = Path.Combine(wwwRootPath + "/image", fileName);
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await empleado.ArchivoImagen.CopyToAsync(fileStream);
                }
                #endregion
            }


            if (id != empleado.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(empleado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmpleadoExists(empleado.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(empleado);
        }

        // GET: Empleados/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var empleado = await _context.empleados
                .FirstOrDefaultAsync(m => m.id == id);
            if (empleado == null)
            {
                return NotFound();
            }

            return View(empleado);
        }

        // POST: Empleados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var empleado = await _context.empleados.FindAsync(id);
            _context.empleados.Remove(empleado);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmpleadoExists(int id)
        {
            return _context.empleados.Any(e => e.id == id);
        }

        #region Visualiza PDF
        public IActionResult PrintPdf()
        {
            return new ViewAsPdf("Details")
            {

            };
        }
        [Route("ruta1")]
        public async Task<IActionResult> PrintPdf(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var empleado = await _context.empleados
                .FirstOrDefaultAsync(m => m.id == id);
            if (empleado == null)
            {
                return NotFound();
            }
            return new ViewAsPdf("EmpleadoPdf", empleado)
            {

            };
        }
        #endregion
    }
}
